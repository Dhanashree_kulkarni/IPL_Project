package com.iplProject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

public class Main {
    private static final int MATCH_ID = 0;
    private static final int MATCH_SEASON = 1;
    private static final int MATCH_CITY = 2;
    private static final int MATCH_DATE = 3;
    private static final int MATCH_TEAM_1 = 4;
    private static final int MATCH_TEAM_2 = 5;
    private static final int MATCH_TOSS_WINNER = 6;
    private static final int MATCH_TOSS_DECISION = 7;
    private static final int MATCH_RESULT = 8;
    private static final int MATCH_DL_APPLIED = 9;
    private static final int MATCH_WINNER = 10;
    private static final int MATCH_WON_BY_RUNS = 11;
    private static final int MATCH_WON_BY_WICKETS = 12;
    private static final int MATCH_PLAYER_OF_MATCH = 13;
    private static final int MATCH_VENUE = 14;
    private static final int MATCH_UMPIRE_1 = 15;
    private static final int MATCH_UMPIRE_2 = 16;
    private static final int MATCH_UMPIRE_3 = 17;
    private static final int DELIVERY_MATCH_ID = 0;
    private static final int DELIVERY_INNINGS = 1;
    private static final int DELIVERY_BATTING_TEAM = 2;
    private static final int DELIVERY_BOWLING_TEAM = 3;
    private static final int DELIVERY_OVER = 4;
    private static final int DELIVERY_BALL = 5;
    private static final int DELIVERY_BATSMAN_NAME = 6;
    private static final int DELIVERY_NON_STRIKER = 7;
    private static final int DELIVERY_BOWLER_NAME = 8;
    private static final int DELIVERY_SUPER_OVER = 9;
    private static final int DELIVERY_WIDE_RUN = 10;
    private static final int DELIVERY_BYE_RUN = 11;
    private static final int DELIVERY_LEG_BYE_RUN = 12;
    private static final int DELIVERY_NO_BALL_RUN = 13;
    private static final int DELIVERY_PENALTY_RUN = 14;
    private static final int DELIVERY_BATSMAN_RUN = 15;
    private static final int DELIVERY_EXTRA_RUN = 16;
    private static final int DELIVERY_TOTAL_RUN = 17;

    public static void main(String[] args) {
        List<Match> matches = retrieveMatchesData();
        List<Delivery> deliveries = retrieveDeliveriesData();

        findTotalNumberOfMatchesPlayedPerYear(matches);
        findNumberOfMatchesWonPerTeam(matches);
        findExtraRunsConcededPerTeamFor2016(matches, deliveries);
        findEconomicalBowlerFor2015(matches, deliveries);
        findNumberOfMatchesPlayedInEachCity(matches);
    }

    private static List<Match> retrieveMatchesData() {
        List<Match> matchesData = null;

        try (BufferedReader br = new BufferedReader(new FileReader("/home/dhana_k/MB/IPL/DataSets/matches.csv"))) {
            matchesData = new ArrayList<Match>();
            String str = br.readLine();
            while ((str = br.readLine()) != null) {
                String[] fields = str.split(",");

                Match match = new Match();
                match.setMatchId(Integer.parseInt(fields[MATCH_ID]));
                match.setSeason(Integer.parseInt(fields[MATCH_SEASON]));
                match.setCity(fields[MATCH_CITY]);
                match.setDate(fields[MATCH_DATE]);
                match.setTeam1(fields[MATCH_TEAM_1]);
                match.setTeam2(fields[MATCH_TEAM_2]);
                match.setTossWinner(fields[MATCH_TOSS_WINNER]);
                match.setTossDecision(fields[MATCH_TOSS_DECISION]);
                match.setResult(fields[MATCH_RESULT]);
                match.setDlApplied(Integer.parseInt(fields[MATCH_DL_APPLIED]));
                match.setWinner(fields[MATCH_WINNER]);
                match.setWinByRuns(Integer.parseInt(fields[MATCH_WON_BY_RUNS]));
                match.setWinByWickets(Integer.parseInt(fields[MATCH_WON_BY_WICKETS]));
                match.setPlayerOfMatch(fields[MATCH_PLAYER_OF_MATCH]);
                match.setVenue(fields[MATCH_VENUE]);
                if (fields.length > 15)
                    match.setUmpire1(fields[MATCH_UMPIRE_1]);
                if (fields.length > 16)
                    match.setUmpire2(fields[MATCH_UMPIRE_2]);
                if (fields.length > 17)
                    match.setUmpire3(fields[MATCH_UMPIRE_3]);

                matchesData.add(match);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return matchesData;
    }

    private static List<Delivery> retrieveDeliveriesData() {
        List<Delivery> deliveriesData = null;

        try (BufferedReader br = new BufferedReader(new FileReader("/home/dhana_k/MB/IPL/DataSets/deliveries.csv"))) {
            deliveriesData = new ArrayList<Delivery>();
            String str = br.readLine();
            while ((str = br.readLine()) != null) {
                String[] fields = str.split(",");

                Delivery deliveries = new Delivery();
                deliveries.setMatchId(Integer.parseInt(fields[DELIVERY_MATCH_ID]));
                deliveries.setInnings(Integer.parseInt(fields[DELIVERY_INNINGS]));
                deliveries.setBattingTeam(fields[DELIVERY_BATTING_TEAM]);
                deliveries.setBowlingTeam(fields[DELIVERY_BOWLING_TEAM]);
                deliveries.setOver(Integer.parseInt(fields[DELIVERY_OVER]));
                deliveries.setBall(Integer.parseInt(fields[DELIVERY_BALL]));
                deliveries.setBatsmanName(fields[DELIVERY_BATSMAN_NAME]);
                deliveries.setNonStriker(fields[DELIVERY_NON_STRIKER]);
                deliveries.setBowler(fields[DELIVERY_BOWLER_NAME]);
                deliveries.setIsSuperOver(Integer.parseInt(fields[DELIVERY_SUPER_OVER]));
                deliveries.setWideRuns(Integer.parseInt(fields[DELIVERY_WIDE_RUN]));
                deliveries.setByeRuns(Integer.parseInt(fields[DELIVERY_BYE_RUN]));
                deliveries.setLegByeRuns(Integer.parseInt(fields[DELIVERY_LEG_BYE_RUN]));
                deliveries.setNoballRuns(Integer.parseInt(fields[DELIVERY_NO_BALL_RUN]));
                deliveries.setPenaltyRuns(Integer.parseInt(fields[DELIVERY_PENALTY_RUN]));
                deliveries.setBatsmanRuns(Integer.parseInt(fields[DELIVERY_BATSMAN_RUN]));
                deliveries.setExtraRuns(Integer.parseInt(fields[DELIVERY_EXTRA_RUN]));
                deliveries.setTotalRuns(Integer.parseInt(fields[DELIVERY_TOTAL_RUN]));
                if (fields.length > 18)
                    deliveries.setPlayerDismissed(fields[18]);
                if (fields.length > 19)
                    deliveries.setDismissalKind(fields[19]);
                if (fields.length > 20)
                    deliveries.setFielder(fields[20]);

                deliveriesData.add(deliveries);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deliveriesData;
    }

    private static void findTotalNumberOfMatchesPlayedPerYear(List<Match> matches) {
        Map<Integer, Integer> matchesPlayedPerYearMap = new LinkedHashMap<Integer, Integer>();

        for (Match match : matches) {
            int yearOfMatch = match.getSeason();
            if (matchesPlayedPerYearMap.containsKey(yearOfMatch)) {
                matchesPlayedPerYearMap.put(yearOfMatch, matchesPlayedPerYearMap.get(yearOfMatch) + 1);
            } else matchesPlayedPerYearMap.put(yearOfMatch, 1);
        }
        System.out.println("Finding Number of matches played per year of all the years in IPL");
        matchesPlayedPerYearMap.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
    }

    private static void findNumberOfMatchesWonPerTeam(List<Match> matches) {
        Map<String, Integer> matchesWonPerTeamMap = new LinkedHashMap<String, Integer>();

        for (Match match : matches) {
            String winner = match.getWinner();
            if (matchesWonPerTeamMap.containsKey(winner)) {
                if (!winner.isEmpty()) {
                    matchesWonPerTeamMap.put(winner, matchesWonPerTeamMap.get(winner) + 1);
                }
            } else matchesWonPerTeamMap.put(winner, 1);
        }
        System.out.println("Finding Number of matches won of all teams over all the years of IPL");
        matchesWonPerTeamMap.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
    }

    private static void findExtraRunsConcededPerTeamFor2016(List<Match> matches, List<Delivery> deliveries) {
        Map<String, Integer> runsConcededMap = new HashMap<String, Integer>();

        for (Match matchObj : matches) {
            if (matchObj.getSeason() == 2016) {
                for (Delivery deliverysObj : deliveries) {
                    if (deliverysObj.getMatchId() == matchObj.getMatchId()) {
                        if (runsConcededMap.containsKey(deliverysObj.getBattingTeam())) {
                            runsConcededMap.put(deliverysObj.getBattingTeam(), runsConcededMap.get(deliverysObj.getBattingTeam()) + deliverysObj.getExtraRuns());
                        } else runsConcededMap.put(deliverysObj.getBattingTeam(), deliverysObj.getExtraRuns());
                    }
                }
            }

        }
        System.out.println("Finding extra runs conceded per team, for the year 2016");
        runsConcededMap.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
    }

    private static void findEconomicalBowlerFor2015(List<Match> matches, List<Delivery> deliveries) {
        Map<String, Integer> runsMap = new HashMap<String, Integer>();
        Map<String, Double> oversMap = new HashMap<String, Double>();
        Map<String, Double> economyOfBowlerMap = new LinkedHashMap<String, Double>();


        for (Match match : matches) {
            if (match.getSeason() == 2015) {
                for (Delivery deliveriesObj : deliveries) {
                    if (deliveriesObj.getMatchId() == match.getMatchId()) {
                        if (runsMap.containsKey(deliveriesObj.getBowler())) {
                            runsMap.put(deliveriesObj.getBowler(), runsMap.get(deliveriesObj.getBowler()) + deliveriesObj.getTotalRuns());
                        } else runsMap.put(deliveriesObj.getBowler(), deliveriesObj.getTotalRuns());
                    }
                }
            }
        }

        for (Match match : matches) {
            if (match.getSeason() == 2015) {
                for (Delivery deliveriesObj : deliveries) {
                    if (deliveriesObj.getMatchId() == match.getMatchId()) {
                        if (oversMap.containsKey(deliveriesObj.getBowler())) {
                            oversMap.put(deliveriesObj.getBowler(), oversMap.get(deliveriesObj.getBowler()) + 1.0);
                        } else oversMap.put(deliveriesObj.getBowler(), 1.0);
                    }
                }
            }
        }

        String[] bowler = oversMap.keySet().toArray(new String[0]);
        for (String s : bowler) {
            if (oversMap.containsKey(s)) {
                double runs = oversMap.get(s);
                oversMap.put(s, (runs / 6));
            }
        }
        for (String s : bowler) {
            double economyRate = runsMap.get(s) / oversMap.get(s);
            economyOfBowlerMap.put(s, economyRate);
        }
        System.out.println("Finding the top economical bowlers for the year 2015");
        economyOfBowlerMap.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
    }

    private static void findNumberOfMatchesPlayedInEachCity(List<Match> matches) {
        Map<String, Integer> matchesPlayedPerYearMap = new HashMap<String, Integer>();

        for (Match match : matches) {
            String cityOfMatch = match.getCity();
            if (matchesPlayedPerYearMap.containsKey(cityOfMatch)) {
                matchesPlayedPerYearMap.put(cityOfMatch, matchesPlayedPerYearMap.get(cityOfMatch) + 1);
            } else matchesPlayedPerYearMap.put(cityOfMatch, 1);
        }
        System.out.println("Finding Number of matches played in each city");
        matchesPlayedPerYearMap.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
    }
}
